# TP #8 - Artéfacts

> **Objectifs du TP**
> * Exporter le rapport de couverture de code sous forme d'artéfact et le télécharger
>
> **Prérequis**
> Avant de commencer ce TP, vous devez avoir satisfait les prérequis suivants
> * Vous avez validé le TP #6 bonus
>
> **Niveau de difficulté :** Intermédiaire

## Répertoire de travail

Nous allons commencer par nous assurer que nous sommes bien dans notre répertoire de travail `/home/ubuntu/sophie/ci-app`.

```bash
$ cd ~/sophie/ci-app
$ pwd
```

## Ajout du rapport de couverture de code

Nous souhaitons que la couverture de code soit exportée sous format HTML, nous allons suivre cette [documentation](https://pytest-cov.readthedocs.io/en/latest/reporting.html).

Si les dépendances du TP #6 ont bien été installées, il n'est pas nécessaire d'en ajouter de nouvelles. Veillez tout de même à avoir bien les dépendances installées en local !

```shell
pip3 install -r requirements.txt
```

Pour générer un rapport de couverture de code avec `pytest` au format HTML, on peut simplement utiliser la commande :

```shell
python3 -m pytest --junit-xml=pytest-report.xml --cov=./ --cov-report=html --cov-report=term
```

Lancer cette commande sur votre machine de travail.

> Questions 8.1
>
> - Où ont été générés les fichiers du rapport ?
> - Comment peut-on les lire ?
> - À quoi sert le `--cov-report=term` ?
> - Pourquoi lancer avec `python3` ici plutôt que `python` comme dans les jobs ?
> 
> Réponses 
> - Les fichiers ont été générés par défaut dans le dossier `htmlcov`.
> - On peut simplement ouvrir avec son navigateur le fichier `index.html` qui se trouve dans le dossier généré et consulter le rapport.
> - Cela permet de conserver l'output du code coverage sur la sortie du terminal et conserver les fonctionnalités de parsing du code coverage offertes par Gitlab et mises en place sur le TP #8.
> - Au moment de la rédaction du TP, nous utilisons une Ubuntu 18.04 où pour utiliser Python 3, il faut utiliser `python3`. 
> Ce n'est pas le même environnement que celui des jobs qui tournent dans une image Docker où seul Python 3 existe et `python` pointe directement vers Python 3 !

Mettre à jour le job `Launch Unit Tests` de manière à ce que notre runner Gitlab CI génère également ce rapport.

> Questions 8.2
>
> - Où se trouvent les fichiers du rapport générés lors du job Gitlab CI
> - Comment peut-on y accéder ?
> 
> Réponses 
> - Les fichiers ont été générés dans le conteneur instancié pour lancer les tests.
> - On ne peut pas ! Une fois le job terminé, le conteneur est détruit ainsi que tout ce qu'il contient.

## Mise à disposition du rapport via un artéfact

On a vu qu'il n'était pas possible pour nous de récupérer 

Dans le fichier `.gitlab-ci.yml`, on peut préciser un artéfact à générer lors du job `Launch Unit Tests` : 

```yaml
Launch Unit Tests:
  stage: unittests
  image: $PYTHON3_IMAGE
  before_script:
    - apk add --no-cache gcc musl-dev
  script:
  - pip install -r requirements.txt
  - python -m pytest --junit-xml=pytest-report.xml --cov=./ --cov-report=html --cov-report=term
  artifacts:
    paths:
      - htmlcov
```

Soumettre ces changements et observer le résultat du job `Launch Unit Tests`. Vous devriez observer lorsqu'il se termine cette fenêtre supplémentaire sur la droite du job :

![](./.assets/artifacts-gitlab.png)

Télécharger et explorer le rapport produit pour retrouver le détail de la couverture de code.

> Questions 8.3
>
> - Quel est le temps de conservation des artéfacts par défaut ?
> - Comment le modifier ?

Gitlab CI bénéficie aussi d'une fonctionnalité qui permet de parser automatiquement les rapports à partir du moment où ils sont produits dans un format standard (liste [ici](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreports)). 
Cela permet de visualiser directement le détail dans une MR et est plus adapté au cas d'usage que nous avons ici.

Dans le fichier `.gitlab-ci.yml`, rajouter un `artifact.report` au format JUnit qui pointe vers le XML produit par `pytest` dans le job `Launch Unit Tests` : 

```yaml
Launch Unit Tests:
  stage: unittests
  image: $PYTHON3_IMAGE
  before_script:
    - apk add --no-cache gcc musl-dev
  script:
  - pip install -r requirements.txt
  - python -m pytest --junit-xml=pytest-report.xml --cov=./ --cov-report=html --cov-report=term
  artifacts:
    paths:
      - htmlcov
    reports:
      junit: pytest-report.xml
```

Soumettre ces changements et faire une MR pour merger le code sur `master`.

Vous devriez obtenir cette écrans sur la MR : 

![](./.assets/test-report-mr.png)

Et celui-ci dans le lien "View full report"

![](./.assets/test-report-pipeline.png)

Fin du TP #8 bonus.
