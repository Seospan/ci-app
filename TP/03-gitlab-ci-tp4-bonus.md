# TP #4 - Utilisation des API Gitlab-CI

> **Objectifs du TP**
> * Découvrir comment utiliser les API de Gitlab-CI.
> * Réussir à déclencher un pipeline via l'API et récupérer son résultat
>
> **Prérequis**
> Aucun prérequis
>
> **Niveau de difficulté :** Débutant
> 
> **Temps estimé :** 40 min


## Exécution conditionnelle du job de construction de l'image Docker

Dans ce TP, notre objectif va être de réussir à lancer le pipeline créé précédemment depuis un autre pipeline.

Vous devrez :
- créer et gérer vos tokens.
- mettre en place un trigger sur votre pipeline.
- déclencher votre appel de CI depuis votre autre CI à l'aide des appels d'API.
- récupérer le résultat du pipeline déclenché une fois celui-ci terminé.

## Mise en place de l'environnement de travail

Afin de créer un pipeline dans un autre projet, nous allons avoir besoin de créer un second projet que l'on va
nommer `test-trigger-pipeline`.

Une fois créé, on va le cloner sur notre environnement (attention à bien être en dehors du dossier ci-app).

## Premier pas avec l'API Gitlab

Notre premier objectif va être de générer une Personal Access Key afin d'accéder à nos API. Pour ce faire, il faut se rendre
sur le site de Gitlab puis aller dans *Setting* > *Access Tokens* et générer un token avec les permissions suivantes :

- `read_api`

> Note : par soucis de sécurité, nous restraignons au maximum les droits que l'on donne à cette utilisateur.

On va ensuite pouvoir identifier notre projet. Pour ce faire, faisons un premier appel pour lister tous les projets :

```bash
$ curl --header "Private-Token: <your_access_token>" https://gitlab.com/api/v4/projects\?owned\=yes
```

Pour une output plus lisible, nous vous recommandons de mettre `| jq .` à la fin de la ligne. Cela formattera le json pour le rendre lisible. (S'il n'est pas présent sur les environnements n'hésitez pas à l'installer)

Plus d'option de selection sont disponible [ici](https://docs.gitlab.com/ee/api/projects.html#list-all-projects).

> Question :
>
> Quelle est l'id de notre projet ? Notez le quelque part car nous en aurons besoin pour la suite de cet exercice.

Maintenant notre id en poche, nous allons vouloir lister tous les pipelines déclencher dans notre projet `ci-app`. Pour ce faire, nous
pouvons utiliser la commande `curl` suivante :

```bash
$ curl --header "Private-Token: <your_access_token>" https://gitlab.com/api/v4/projects/<project_id>/pipelines
```

> Question :
>
> Quels champs sont présents dans le json de réponse ?
> En vous basant sur la logique de l'API, quelle commande faut-il lancer pour récuperer uniquement le dernier pipeline en date ? ([Indice](https://docs.gitlab.com/ee/api/pipelines.html))

## Trigger de pipeline

Nous avons compris comment utiliser l'API de Gitlab. Maintenant, passons aux choses sérieuses : déclenchons un pipeline depuis un autre pipeline.

La première chose dont nous allons avoir besoin est d'un trigger token. Pour ce faire, dans le projet `ci-app`, allez dans **Settings** > **CI / CD** > **Pipeline triggers** et générer un token.

On va pouvoir voir si le token fonctionne en lançant la commande :
```bash
$ curl -X POST -F token=<trigger_token> -F ref=master https://gitlab.com/api/v4/projects/<project_id>/trigger/pipeline
```

> Question :
>
> Que nous renvoi cette commande ?

Essayons maintenant de l'exploiter au sein de notre pipeline.

Pour ce faire, créons un `job` dans notre projet qui va contenir notre curl. Bien évidemment, pour des raisons de sécurité évidente,
__le token devra être stocké dans un secret__.

Normalement si tout c'est bien passé, vous devriez avoir un pipeline qui ressemble à ça :

<details>
  <summary>Code</summary>

  ```yaml
  stages:
    - trigger
  
  trigger_main_project_pipeline:
  stage: trigger
  script:
    - curl -X POST -F token=$SECRET_TOKEN -F ref=master https://gitlab.com/api/v4/projects/<project_id>/trigger/pipeline
  ```
</details>

> Note :
>
> Il est possible de mettre un filtre lors de l'exécution d'un job afin que
> celle-ci ne soit jouer que si le pipeline a été déclenché à l'aide d'un trigger. ([Je veux en savoir plus](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic).)

Félicitations, vous savez désormais utiliser l'API de Gitlab-CI et déclencher des pipelines depuis d'autre pipeline.

Fin du TP.
